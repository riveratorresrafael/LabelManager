import psycopg2
import pandas as pd
import datetime

GROUP_CODE = 0
TEXT_KEY = 1
EN = 2
ES = 3

def process():
    """ Connect to the PostgreSQL database server """
    conn = None
    try:
        print("Enter schema to update:")
        schema = input()

        if schema != 'dev' and schema != 'test':
            print('There is not q schema with that name!')
            return 0

        queryText1 = "select platform||group_code||text_key,en,es from "+schema+".labels_localized where platform = %s"
        queryText2 = "insert into "+schema+".labels_localized(en,es,platform,group_code,text_key,created_date) values (%s,%s,%s,%s,%s,current_date)"
        queryText3 = "update "+schema+".labels_localized set en=%s, es=%s, created_date=current_date where platform=%s and group_code=%s and text_key=%s"

        #print("Enter platform to update:")
        #platform = input()
        platform = 'app'

        if platform != 'app' and platform != 'web':
            print('There is not a platform with that name!')
            return 0

        print('Finding excel with data...')
        excel = pd.read_excel(platform+'.xlsx').values

        labels = []
        for cell in excel:
            row = {
                'group_code': cell[0],
                'text_key': cell[1],
                'en': cell[2],
                'es': cell[3]
            }
            labels.append(tuple([cell[EN], cell[ES], platform, cell[GROUP_CODE], cell[TEXT_KEY]]))
        #print(labels)
        #print(len(labels))

        print('Connecting to the PostgreSQL database...')
        conn = psycopg2.connect(
            host="5fde368f-4ac5-44b1-b3a8-2d31efc1a9e9.budepemd0im5pmu4u60g.databases.appdomain.cloud",
            port="31197",
            database="ibmclouddb",
            user="ibm_cloud_0bbbd636_10f1_4f8b_b0ff_2d7b314e086b",
            password="a45857328e891df4d85629089919d2021204ef8deee6d9d5a32ee3abdb8db7be")

        cursor = conn.cursor()
        print('Reading current labels...')
        cursor.execute(queryText1, (platform,))
        current_labels = cursor.fetchall()
        #print(current_labels)

        print('Looking for changes...')
        to_update = []
        to_add = []
        found = False
        for label in labels:
            key = platform+label[3]+label[4]
            for current_label in current_labels:
                if current_label[0] == key:
                    found = True
                    if current_label[1] != label[0] or current_label[2] != label[1]:
                        to_update.append(label)
                    break
            if not found:
                to_add.append(label)
            found = False

        #print(to_update)
        #print("toUpdate",len(to_update))

        #print(to_add)
        #print("toAdd",len(to_add))

        cursor.executemany(queryText2, to_add)
        conn.commit()
        print(cursor.rowcount, "labels CREATED!")

        cursor.executemany(queryText3, to_update)
        conn.commit()
        print(cursor.rowcount, "labels UPDATED!")

    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
        return 0
    finally:
        if conn is not None:
            cursor.close()
            conn.close()
            print('Database connection closed...')
            return 1


if __name__ == '__main__':
    code = process()
    print("Process finished with code ",code)
    print("Press ENTER key to finish...")
    input()